---
date: 2022-11-26T11:15:58-04:00
description: ""
featured_image: "https://cdn-icons-png.flaticon.com/512/4004/4004113.png"
tags: []
title: "Parcours scolaire"
---

J'ai étudié à Lannion en bretagne, plus précisément dans le département des côtes d'armor (22) au collège et Lycée St-Joseph Bossuet.

En première j'ai emprunté une voie spéciale avec les spécialités Mathématiques Informatiques et Histoire Géographie Science Politique.

Ensuite j'ai obtenu mon bac en gardant les spécialités Informatiques et HGGSP et maths complémentaires.
Bac que j'ai obtenu mention bien en Juillet 2022.

Ensuite, je suis allé à l'IUT de Vannes pour me perfectionner dans l'informatique, ma passion.
